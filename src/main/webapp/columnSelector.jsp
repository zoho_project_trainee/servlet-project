<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.List" %>
<%@ page import = "com.genericSort.Model.dbOperations"%>
<%@ page import = "com.genericSort.Controller.logoutController"%>
<%@ taglib prefix ="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix ="fn" uri ="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="tableSorterStyle.css" rel="stylesheet">
<title></title>
</head>
<body>
 	<div class = "column-viewer">
 <%

	logoutController.clearBrowserCache(response);
 	if(session.getAttribute("status") != "login"){
		response.sendRedirect("index.html");
	}
 	else {
 	dbOperations getDatas = dbOperations.getInstance();
 	String tableName = request.getParameter("tableName");
 	List<String> columnNames = getDatas.getColumnNames(tableName);
 	request.setAttribute("columnNames",columnNames);
 	request.setAttribute("tableName",tableName);
 	}
%>
	
	<c:if test="${ not empty columnNames}">
		<form action = "columnViewController" class = "column-list" method = "get">
		<div class = "columns-container">
			<ul>
				<c:forEach var ="index" begin = "0" end = "${fn:length(columnNames)-1}">
					<li>
						<label for = "checkBox" class = "column-Name"${index}>${columnNames.get(index)}</label>
						<input type = "checkbox" id = "checkBox"${index} name = "checkBoxGroup" value = ${columnNames.get(index) } >
					</li>
				</c:forEach>
			</ul>
		</div>
			<input type="hidden" name="tableName" value="${tableName}">
			<div class = "radioBtn-container">
				<li class = "view-list"><input type="radio" name="view-type" class ="selector" value="sorted" checked>Sorted</li>
				<li class = "view-list"><input type="radio" name="view-type" class ="selector" value="unsorted">UnSorted</li>
			</div>
			<input type = "submit" value = "View Column" class = "submit-btn">
		</form>
	</c:if>
 	</div>
</body>
</html>