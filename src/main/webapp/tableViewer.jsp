<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.List" %>
<%@ page import = "com.genericSort.Model.dbOperations"%>
<%@ page import = "com.genericSort.Controller.logoutController"%>
<%@ page import = "com.genericSort.Sorter"%>
<%@ taglib prefix ="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix ="fn" uri ="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="tableSorterStyle.css" rel="stylesheet">
<title></title>
</head>
<body>
	<%
	logoutController.clearBrowserCache(response);
	if(session.getAttribute("status") != "login"){
		response.sendRedirect("index.html");
	}
	else{
	String tableName = (String) session.getAttribute("tableName");
	String[] selectedColumns = (String[]) session.getAttribute("columnNames");
	String selection = (String) session.getAttribute("selection");
	int totalPages = (int) session.getAttribute("totalPages");
	
	List<List<?>> columnValues = (List<List<?>>) session.getAttribute("columnValues");
	}
	%>
	
	<div class = "table-container">
	<c:choose>
		<c:when test="${not empty columnNames}">
		<table>
		    <thead>
		        <tr>
		            <c:forEach var="columnName" items="${columnNames}">
		                <th>${columnName}</th>
		            </c:forEach>
		        </tr>
		    </thead>
		    <tbody>
		        <c:forEach var="i" begin="0" end="${fn:length(columnValues[0])-1}">
		            <tr>
		                <c:forEach var="columnIndex" begin="0" end="${fn:length(columnNames)-1}">
		                    <td>${columnValues[columnIndex][i]}</td>
		                </c:forEach>
		            </tr>
		        </c:forEach>
		    </tbody>
	   </table>
	   <div class = "next-page">
		   <c:forEach var = "i" begin = "1" end = "${totalPages}">
		   		<ul>
				 <form action = "tableViewController" method = "get">
				     <li class = "pageNo-list">
				     	<input type = "submit" name="page" value = "${i}" class = "pageNo" >
				     </li>
				 </form>
				</ul>
		  </c:forEach>
	    </div>
	   </c:when> 
	   <c:otherwise>
	   	<h2>No Columns Selected</h2>
	   </c:otherwise>
   </c:choose>
   </div>
</body>
</html>