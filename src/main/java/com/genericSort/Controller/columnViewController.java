package com.genericSort.Controller;

import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


public class columnViewController extends HttpServlet {
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession(false);
		
		String[] selectedColumns = request.getParameterValues("checkBoxGroup");
		String tableName = request.getParameter("tableName");
		String selection = request.getParameter("view-type");
		
		session.setAttribute("tableName",tableName);
		session.setAttribute("columnNames", selectedColumns);
		session.setAttribute("selection",selection);
		
		response.sendRedirect("tableViewController?page=1");
	}

}
