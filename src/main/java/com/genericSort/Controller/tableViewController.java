package com.genericSort.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.genericSort.Sorter;
import com.genericSort.Model.dbOperations;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/tableViewController")
public class tableViewController extends HttpServlet{
	
	public void doGet(HttpServletRequest request , HttpServletResponse response) throws IOException {
	
		HttpSession session = request.getSession(false);
		
		int pageId = Integer.parseInt(request.getParameter("page"));
		
		/// 5 is the limit of rows to be displayed.
		pageId = (pageId-1)*5;
	
	    String[] selectedColumns = (String[]) session.getAttribute("columnNames");
		String tableName = (String) session.getAttribute("tableName");
		String selection = (String) session.getAttribute("selection");
		
		
		List<List<?>> columnValues = addColumnValuesToList(selectedColumns,tableName,pageId);
		columnValues = sortIfNeeded(columnValues,selection);
		int totalPages = getTotalPages(tableName);
		
		session.setAttribute("columnValues", columnValues);
		session.setAttribute("totalPages",totalPages);
		
		response.sendRedirect("tableViewer.jsp");
		
	}
	
	private int getTotalPages(String tableName) {
		dbOperations getDatas = dbOperations.getInstance();
		int totalRows = getDatas.getNoOfRows(tableName);
		return (totalRows%5!=0) ? (totalRows/5+1) : (totalRows/5);
	}
	
	private List<List<?>> addColumnValuesToList(String[] selectedColumns,String tableName,int pageId){
		List<List<?>> columnValues = new ArrayList<>();
		dbOperations getDatas = dbOperations.getInstance();
		if(selectedColumns!=null){
			columnValues = getDatas.getColumnsData(selectedColumns,tableName,pageId);
		}
		return columnValues;
	}
	
	private List<List<?>> sortIfNeeded(List<List<?>> listOfColumnValues,String selection)
	{
		if(selection.equals("sorted")){
			return Sorter.sort(listOfColumnValues);
		}
		return listOfColumnValues;
	}
}
