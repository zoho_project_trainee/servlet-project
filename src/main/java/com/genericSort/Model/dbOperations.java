package com.genericSort.Model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class dbOperations <T> {
	/// Variables
    final static String URL = "jdbc:postgresql://localhost:5454/";
    final static String DB_NAME ="GenericSort";
    final static String USERNAME = "postgres";
    final static String PASSWORD = "SKatk1611#";
    
    static Connection connection = null;
    private static dbOperations instance;
    static List<String> records = new ArrayList<>();
    List<List<T>> columnDataList = new ArrayList<>();
    
    /// Connection Methods
    
    public static dbOperations getInstance() {
    	if(instance == null) {
    		instance = new dbOperations();
    	}
    	return instance;
    }
    
    private static void openConnection()
    {
        try {
        	Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL + DB_NAME, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void closeConnection()
    {
        try {
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    
    /// DB Access Methods
    
    public List<String> getTableNames(){
    	String query = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';";
    	Statement statement;
    	ResultSet resultSet;
    	records.clear();
    	openConnection();
    	try {
    		statement = connection.createStatement();
    		resultSet = statement.executeQuery(query);
    		while(resultSet.next()) {
    			records.add(resultSet.getString("table_name"));
    		}
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}
    	closeConnection();
    	return records;
    }
    
    public List<String> getColumnNames(String tableName){
    	String query = "SELECT column_name FROM information_schema.columns WHERE table_name = '"+tableName+"';";
    	Statement statement;
    	ResultSet resultSet;
    	records.clear();
    	openConnection();
    	try {
    		statement = connection.createStatement();
    		resultSet = statement.executeQuery(query);
    		while(resultSet.next()) {
    			records.add(resultSet.getString("column_name"));
    		}
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}
    	closeConnection();
    	return records;
    }
    
    public List<List<T>> getColumnsData(String[] columnNames,String table,int id){
    	openConnection();
    	columnDataList.clear();
    	for(String columnName : columnNames) {
    		List<T> columnData = new ArrayList<>();
	    	String query = "SELECT "+columnName+" FROM "+table+" LIMIT 5 OFFSET "+id+";";
	    	Statement statement;
	    	ResultSet resultSet;
	    	try {
	    		statement = connection.createStatement();
	    		resultSet = statement.executeQuery(query);
	    		while(resultSet.next()) {
	    			columnData.add((T)resultSet.getObject(1));
	    		}
	    	}catch(SQLException e) {
	    		e.printStackTrace();
	    	}
	    	columnDataList.add(columnData);
    	}
	    	closeConnection();
    	return columnDataList;
    }
    
    public int getNoOfRows(String tableName) {
    	int totalRows = 0;
    	openConnection();
    	try {
    		String query = "SELECT COUNT(*) FROM "+tableName+" ;";
    		Statement statement;
    		ResultSet resultSet;
    		
    		statement = connection.createStatement();
    		resultSet = statement.executeQuery(query);
    		
    		if(resultSet.next()) {
    			totalRows = resultSet.getInt(1);
    		}
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}
    	closeConnection();
    	return totalRows;
    }
    
}
