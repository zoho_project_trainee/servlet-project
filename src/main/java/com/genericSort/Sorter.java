package com.genericSort;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Sorter
{
    public static List<List<?>> sort(List<List<?>> unsortedList)
    {
    	List<List<?>> sorted = new ArrayList<>();
    	
    	for(int index=0;index<unsortedList.size();index++) {
    		
    		List<?> listToSort = unsortedList.get(index);
	        if(listToSort.get(0) instanceof Integer)
	        {
	            Collections.sort((ArrayList<Integer>)listToSort, new Comparator<Integer>() {
	                @Override
	                public int compare(Integer t1, Integer t2) {
	                    return t1>t2 ? 1 : t1<t2 ? -1 : 0;
	                }
	            });
	            sorted.add(listToSort);
	        }
	
	        else if(listToSort.get(0) instanceof Float)
	        {
	            Collections.sort((ArrayList<Float>)listToSort, new Comparator<Float>() {
	                @Override
	                public int compare(Float t1, Float t2) {
	                    return t1>t2 ? 1 : t1<t2 ? -1 : 0;
	                }
	            });
	            sorted.add(listToSort);
	        }
	
	        else if(listToSort.get(0) instanceof Long)
	        {
	            Collections.sort((ArrayList<Long>)listToSort, new Comparator<Long>() {
	                @Override
	                public int compare(Long t1, Long t2) {
	                    return t1>t2 ? 1 : t1<t2 ? -1 : 0;
	                }
	            });
	            sorted.add(listToSort);
	        }
	
	        else if(listToSort.get(0) instanceof Double)
	        {
	            Collections.sort((ArrayList<Double>)listToSort, new Comparator<Double>() {
	                @Override
	                public int compare(Double t1, Double t2) {
	                    return t1>t2 ? 1 : t1<t2 ? -1 : 0;
	                }
	            });
	            sorted.add(listToSort);
	        }
	
	        else if(listToSort.get(0) instanceof String)
	        {
	            Collections.sort((ArrayList<String>)listToSort, new Comparator<String>() {
	                @Override
	                public int compare(String t1, String t2) {
	                    return t1.length()>t2.length() ? 1 : t1.length()<t2.length() ? -1 : 0;
	                }
	            });
	            sorted.add(listToSort);
	        }
    	}
    	
        return sorted;
    }
}

